#!/usr/bin/env python
import pysam
import os
import nanomath
from nanoget import *
import pandas as pd 
import concurrent.futures as cfutures
from argparse import ArgumentParser, HelpFormatter
__version__ = '1.0.0'

def get_Mismatch(read):
    """Return mismatch of a read.
    based on the NM tag if present, if not calculate from MD tag and CIGAR string"""
    try:
        return read.get_tag("NM")
    except KeyError:
        try:
            return (parse_MD(read.get_tag("MD")) + parse_CIGAR(read.cigartuples))
        except KeyError:
            return None
    except ZeroDivisionError:
        return None

def get_pID(read):
    """Return the percent identity of a read.
    based on the NM tag if present,
    if not calculate from MD tag and CIGAR string
    read.query_alignment_length can be zero in the case of ultra long reads aligned with minimap2 -L
    """
    try:
        return 100 * (1 - get_Mismatch(read) / float(read.query_alignment_length))
    except KeyError:
        return None
    except ZeroDivisionError:
        return None

def parse_MD(MDlist):
    """Parse MD string to get number of mismatches and deletions."""
    return sum([len(item) for item in re.split('[0-9^]', MDlist)])

def parse_CIGAR(cigartuples):
    """Count the insertions in the read using the CIGAR string."""
    return sum([item[1] for item in cigartuples if item[0] == 1])

def parse_CIGAR_In(cigartuples):
    """Count the insertions in the read using the CIGAR string."""
    return sum([item[1] for item in cigartuples if item[0] == 1])

def parse_CIGAR_Del(cigartuples):
    """Count the deletions in the read using the CIGAR string."""
    # Count the deletions cigartuples tag = 2
    return sum([item[1] for item in cigartuples if item[0] == 2])

def query_length(read):
    cigar = extract_cigar(read.cigarstring)
    return sum([cigar[k] for k in cigar if k not in ['D','N'] ])

def get_coverage(read):
    """Calculate aligned read coverage."""
    return 100 * ((read.query_alignment_length)/float(query_length(read)))

def extract_from_read_detail(read):
    """Extracts metrics from bam.
    Worker function per chromosome
    loop over a bam file and create list with tuples containing metrics:
    """
    # extraction
#     query = read.qname
#     quals = nanomath.ave_qual(read.query_qualities)
#     query_len = query_length(read)
#     matches = read.query_alignment_length
#     mismatches = get_Mismatch(read)
#     insertions = parse_CIGAR_In(read.cigartuples)
#     deletions = parse_CIGAR_Del(read.cigartuples)
#     aligned_quals = nanomath.ave_qual(read.query_alignment_qualities)
#     mapQ = read.mapping_quality
#     percentIdentity = get_pID(read)
#     coverage = get_coverage(read)
#     refname = read.reference_name
    
    if read.is_supplementary:
        aligned_type = "supplementary"
    elif read.is_secondary:
        aligned_type = "is_secondary"
    elif read.is_unmapped:
        aligned_type = "unmapped"
    else:
        aligned_type = "primary"

    if read.is_unmapped:
        return (read.qname, "", read.query_length, 
             "", "", "", 
             "", "", 
             "", "", "", "", 
             "", aligned_type)
    else:
        return (read.qname, nanomath.ave_qual(read.query_qualities), query_length(read), 
             read.query_alignment_length, get_Mismatch(read), parse_CIGAR_In(read.cigartuples), 
             parse_CIGAR_Del(read.cigartuples), nanomath.ave_qual(read.query_alignment_qualities), 
             read.mapping_quality, get_pID(read), get_coverage(read), "{}:{}-{}".format(read.reference_name,read.reference_start,read.reference_end), 
             chimeric_check(read), aligned_type)

def extract_from_bam_detail(params):
    """Extracts metrics from bam.
    Worker function per chromosome
    loop over a bam file and create list with tuples containing metrics:
    """
    bam, chromosome = params
    samfile = pysam.AlignmentFile(bam, "rb")
    # extraction
#     query = read.qname
#     quals = nanomath.ave_qual(read.query_qualities)
#     query_len = query_length(read)
#     matches = read.query_alignment_length
#     mismatches = get_Mismatch(read)
#     insertions = parse_CIGAR_In(read.cigartuples)
#     deletions = parse_CIGAR_Del(read.cigartuples)
#     aligned_quals = nanomath.ave_qual(read.query_alignment_qualities)
#     mapQ = read.mapping_quality
#     percentIdentity = get_pID(read)
#     coverage = get_coverage(read)
#     refname = read.reference_name
    return [
        (read.qname, nanomath.ave_qual(read.query_qualities), query_length(read), 
         read.query_alignment_length, get_Mismatch(read), parse_CIGAR_In(read.cigartuples), 
         parse_CIGAR_Del(read.cigartuples), nanomath.ave_qual(read.query_alignment_qualities), 
         read.mapping_quality, get_pID(read), get_coverage(read), "{}:{}-{}".format(read.reference_name,read.reference_start,read.reference_end),
         chimeric_check(read))
        for read in samfile.fetch(reference=chromosome, multiple_iterators=True)
        if not read.is_secondary and not read.is_supplementary and not read.is_unmapped]

def process_bam_detail(bam, **kwargs):
    """Combines metrics from bam after extraction.
    Processing function: calls pool of worker functions
    to extract from a bam file the following metrics:
    Returned in a pandas DataFrame
    """
    samfile = check_bam(bam)
    chromosomes = samfile.references
    params = zip([bam] * len(chromosomes), chromosomes)
    with cfutures.ProcessPoolExecutor() as executor:
        datadf = pd.DataFrame(
            data=[res for sublist in executor.map(extract_from_bam_detail, params) for res in sublist],
            columns=["query", "quals", "query_len", "matches", "mismatches", "insertions",  
                     "deletions", "aligned_quals", "mapQ", "percentIdentity", "coverage", 
                     "target","chimeric"]
        ).dropna()
    return (datadf, samfile.unmapped)

# Fastq handle
def stream_fastq_obj(fastq, threads):
    """Generator for returning metrics extracted from fastq.
    Extract from a fastq file:
    -readname
    -average and median quality
    -read_lenght
    """
    inputfastq = handle_compressed_fastq(fastq)
    with cfutures.ProcessPoolExecutor(max_workers=threads) as executor:
        for results in SeqIO.parse(inputfastq, "fastq"):
            yield results


def extract_cigar(cigarstring):
    import re
    cigar_pat = re.compile(r"(\d+)([MIDNSHP=X])")
    cigarextract = dict()
    for num, tag in cigar_pat.findall(cigarstring):
        cigarextract[tag] = cigarextract.setdefault(tag, 0) + int(num)
    return(cigarextract)

def bam_location(read):
    cigar_count = extract_cigar(read.cigarstring)
    align_size = cigar_count.get('M',0) + cigar_count.get('D',0) + cigar_count.get('N',0)
    if read.is_reverse:
        strand = "-"
    else:
        strand = "+"
    return((read.reference_name,read.reference_start,read.reference_start+align_size, strand))
    
def sa2location(sa_string):
    split_sa = sa_string.split(",")
    cigar_count = extract_cigar(split_sa[3])
    chrom = split_sa[0]
    start = int(split_sa[1])-1
    end = start + cigar_count.get('M',0) + cigar_count.get('D',0) + cigar_count.get('N',0)
    strand = split_sa[2]
    return((chrom, start, end, strand))

def intersect_interval(q, t):
#     print(q)
#     print(t)
    if (q[0] == t[0]):
        if q[2] > t[1] and q[1] < t[2]:
            if q[3] == t[3]:
                return "itself"
            else:
                return "itself_inv"
        else:
            if q[3] == t[3]:
                return "sense"
            else:
                return "antisense"
    else:
        return "elsewhere"

def chimeric_check(read):
    if read.has_tag("SA"):
        pri_interval = bam_location(read)
        sup_interval = sa2location(read.get_tag("SA"))
        chimeric = intersect_interval(pri_interval, sup_interval)
        return chimeric
    else:
        return "None"

## SAM http://yulijia.net/en/bioinformatics/2015/12/21/Linear-Chimeric-Supplementary-Primary-and-Secondary-Alignments.html
## primary alignment = representative 
## secondary alignment = map ambiguously to multiple locations
## supplementary alignment = A chimeric reads but not a representative reads
    

def get_args():
    epilog = """EXAMPLES:
    """
    parser = ArgumentParser(
        description="provide bam summary and extract data of Oxford Nanopore read dataset.",
        epilog=epilog,
        add_help=False)
    general = parser.add_argument_group(
        title='General options')
    general.add_argument("-h", "--help",
                         action="help",
                         help="show the help and exit")
    general.add_argument("-v", "--version",
                         help="Print version and exit.",
                         action="version",
                         version='Nanosummy {}'.format(__version__))
    general.add_argument("-o", "--outdir",
                         help="Specify directory in which output has to be created.",
                         default=".")
    general.add_argument("-p", "--prefix",
                         help="Specify an optional prefix to be used for the output file.",
                         default="",
                         type=str)
    general.add_argument("-n", "--name",
                         help="Specify a custom filename/path for the output, <stdout> for printing to stdout.",
                         default="",
                         type=str)
    general.add_argument("-t", "--threads",
                         help="Set the allowed number of threads to be used by the script.",
                         default=4,
                         type=int,
                         metavar="N")

    inputoptions = parser.add_argument_group(
        title='Input options.')
    inputoptions.add_argument("--readtype",
                              help="Which read type to extract information about from summary. \
                              Options are 1D, 2D, 1D2",
                              default="1D",
                              choices=['1D', '2D', '1D2'])
    target = parser.add_argument_group(
        title="Input data sources, one of these is required.")
    target.add_argument("-stdin","--stdin",
                         help="steam input in sam/bam format",
                         action='store_true')
    target.add_argument("--fastq",
                         help="Data is in one or more default fastq file(s).",
                         nargs='+',
                         metavar="file")
    target.add_argument("--summary",
                         help="Data is in one or more summary file(s) generated by albacore.",
                         nargs='+',
                         metavar="file")
    target.add_argument("--bam",
                         help="Data is in one or more sorted bam file(s). use 'stdin' for stream",
                         nargs='+',
                         metavar="file")
    target.add_argument("--bam_list",
                         help="Data list in file.",
                         type=str,
                         default="")

    output = parser.add_argument_group(
        title='Select Output options.')
    output.add_argument("--matrix_scatter",
                        action='store_true' , 
                        help="Plot scatter matrix for importance score")
    output.add_argument("--fastq_mapped",
                        action='store_true',
                        help='save mapped reads in .mapped.fastq')
    output.add_argument("--fastq_unmapped",
                        action='store_true',
                        help='save unmapped reads in .unmapped.fastq')
    output.add_argument("--data_matrix",
                        action='store_true',
                        help='save data matrix in .data_matrix.txt')
    output.add_argument("--bam_summary",
                        action='store_true',
                        help='save bam summary in .bam_summary.txt')

    filt = parser.add_argument_group(
        title='Filtering options.')
    filt.add_argument("--mapQ",
                        type=int,
                        default=0, 
                        help="Filtering reads by mapQ greater than N (default 0)")
    filt.add_argument("--coverage",
                        type=int,
                        default=0, 
                        help="Filtering reads by percent coverage greater than N (default 0)")
    filt.add_argument("--inverse",
                        action='store_true',
                        help="Inverse result")

    return parser.parse_args()

def bam_mapped2fq(bamF=None, fastqF=None, threads=1, mappedSet=None, mapped_out=True, unmapped_out=True, path="."):
    """Wirte fastq accouding to mapped set
    """
    if mapped_out:
        outf_mapped = open("{}/{}.mapped.fastq".format(path, bamF.replace(".bam","")),"w")
    if unmapped_out:
        outf_unmapped = open("{}/{}.unmapped.fastq".format(path, bamF.replace(".bam","")),"w")

    fastqDf = stream_fastq_obj(fastqF, threads=1)
    for read in fastqDf:
        if mapped_out and read.id in mappedSet:
            outf_mapped.write("{:s}\n".format(read.format('fastq').strip()))
        elif unmapped_out: 
            outf_unmapped.write("{:s}\n".format(read.format('fastq').strip()))
        else:
            continue
    if mapped_out:
        outf_mapped.close()
    if unmapped_out:
        outf_unmapped.close()

def mapped_matrixPlot(df=None, bamF=None, path="."):
    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    df.loc[df.mapQ > 50, 'label'] = "High"
    df.loc[df.mapQ <= 50, 'label'] = "Low"
    colors_palette = {'Low': 'Red', 'High': 'Blue'}
    colors = [colors_palette[c] for c in df['label']]
    pd.plotting.scatter_matrix(df.loc[:,["quals","matches","aligned_quals","mapQ","percentIdentity","coverage"]], 
                                   color=colors, alpha=0.2, figsize=(12, 12), diagonal='kde');
    plt.savefig('{}/{}.mapped_stats.png'.format(path, bamF.replace(".bam","")))
    # plt.savefig('{}.mapped_stats.pdf'.format(bamF))

def main():
    args = get_args()

    if args.stdin:
        columns=["query", "quals", "query_len", "matches", "mismatches", "insertions",  
                     "deletions", "aligned_quals", "mapQ", "percentIdentity", "coverage", 
                     "target","chimeric","aligned_type"]
        infile = pysam.AlignmentFile("-")
        if not args.data_matrix:
            if infile.is_bam:
                outfile = pysam.AlignmentFile("-", "wb", template=infile)
            elif infile.is_sam:
                outfile = pysam.AlignmentFile("-", "w", template=infile)
        for read in infile:
            if args.inverse:
                if read.is_unmapped:
                    if args.data_matrix:
                        data = extract_from_read_detail(read)
                        print("\t".join( map(str, data) ) )
                    else:
                        outfile.write(read)
                elif not read.is_secondary and not read.is_supplementary:
                    data = extract_from_read_detail(read)
                    qdata = dict(zip(columns, data))
                    if qdata['mapQ'] > args.mapQ and qdata['coverage'] > args.coverage:
                        continue
                    else:
                        if args.data_matrix:
                            print("\t".join( map(str, data) ) )
                        else:
                            outfile.write(read)
            else:
                if not read.is_secondary and not read.is_supplementary and not read.is_unmapped:
                    data = extract_from_read_detail(read)
                    qdata = dict(zip(columns, data))
                    if qdata['mapQ'] > args.mapQ and qdata['coverage'] > args.coverage:
                        if args.data_matrix:
                            print("\t".join( map(str, data) ) )
                        else:
                            outfile.write(read)

## fastq
## op.write(">%s\n%s\n+\n%s\n"  % (read.name, read.query_sequence, "".join([chr(ord(x) + 33)) for x in read.query_qualities)

        exit()

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # print(args.fastq)
    # print(args.bam)

    if args.bam_summary:
        outf_summary = open("{}/bam_summary.txt".format(args.outdir), "w")
        outf_summary.write("{}\n".format("\t".join(["bamfile","total_number","number_mapped","number_unmapped","percent_mapped","total_bases","total_mapped_bases"])) )

    if args.bam_list:
        bams = open(args.bam_list).read().strip().split("\n")
    else:
        bams = args.bam
    for i in range(0,len(bams)):
        bamF =bams[i]
        bname = os.path.basename(bamF)

        bamDf, unmapped = process_bam_detail(bamF, threads=args.threads)
        mappedSet = set(bamDf['query'].unique())
        mapped = len(mappedSet)
        if mapped+unmapped > 0:
            print(mapped , unmapped)
            percMapped = 100 * (mapped/(mapped+unmapped))
            print("""Summary:\n {}: total_reads={}, mapped={}, unmapped={}, percent_mapped={:.2f}, total_bases={}, total_mapped_bases={}""".format(bamF, mapped+unmapped, mapped, unmapped, percMapped, sum(bamDf.query_len), sum(bamDf.matches) ))
        ## plot matrix_scatter
        if args.matrix_scatter:
            mapped_matrixPlot(bamDf, bname, path=args.outdir)
        ## write fastq mapped and unmapped
        if args.fastq:
            if len(args.bam) != len(args.fastq):
                exit("number of bam and fastq files are not equal.")
            fastqF = args.fastq[i]
            if args.fastq_mapped and args.fastq_unmapped:
                bam_mapped2fq(bamF=bname, fastqF=fastqF, threads=args.threads, mappedSet=mappedSet, mapped_out=True, unmapped_out=True, path=args.outdir)
            elif args.fastq_mapped:
                bam_mapped2fq(bamF=bname, fastqF=fastqF, threads=args.threads, mappedSet=mappedSet, mapped_out=True, unmapped_out=False, path=args.outdir)
            elif args.fastq_unmapped:
                bam_mapped2fq(bamF=bname, fastqF=fastqF, threads=args.threads, mappedSet=mappedSet, mapped_out=False, unmapped_out=True, path=args.outdir)
        ## export data matrix
        if args.data_matrix:
            bamDf.to_csv("{}/{}.data_matrix.txt".format(args.outdir,bname.replace(".bam","")), sep="\t", index=False)
        ## write bam summary
        if args.bam_summary:
            outf_summary.write( "{}\n".format("\t".join(map(str,[bamF, mapped+unmapped, mapped, unmapped, percMapped, sum(bamDf.query_len), sum(bamDf.matches)]))) )

    if args.bam_summary:
        outf_summary.close()

if __name__ == '__main__':
    main()

