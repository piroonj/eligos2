#!/bin/bash
name=$1
ref=$2

awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$5,0,$4}' <(sed 1d $name) | bedtools slop -b 6 -g ${ref}.fai | bedtools sort | bedtools merge -s | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$1":"$2"-"$3,0,$4}' > $(basename $name .txt).bed

bedtools getfasta -fi ${ref} -bed $(basename $name .txt).bed -fo $(basename $name .txt).fa -s 
