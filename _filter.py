"""
.. module:: filter
   :platform: Unix, MacOSX
   :synopsis: Module for filtering of the Eligos result.

.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com>

"""
#!/usr/bin/env python
import argparse
import pandas as pd
import logging
import os
from _misc import ensure_dir

## Data preparation functions ######################

def get_args():
    example_text = '''Test filter:

    With output as File:
      %(prog)s filter -i file.txt 

     '''
    
    import _option_parsers
    parser = _option_parsers.get_filter_parser()

    return(parser)

def _filter_main(args):
    '''Filter for the Eligos result'''
    import numpy as np
    logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    logging.info('Filter for the Eligos result')
    logging.info(str(args))

    # input
    eligos_result = args.input
    # output
    if args.prefix is None:
        output = "{}".format(os.path.splitext(eligos_result)[0])
    else:
        output = "{}".format(args.prefix)
        ensure_dir(os.path.dirname(output))

    df_inf = pd.read_csv(eligos_result, sep='\t')
    filtering = (df_inf.oddR >= args.oddR) & (df_inf.ESB_test >= args.esb) & (df_inf.total_reads >= args.min_depth) & ((df_inf.pval <= args.pval) & (df_inf.adjPval <= args.adjPval))
    if args.homopolymer:
        filtering = filtering & (df_inf.homo_seq == "--")
    if args.select_base in ["A","T","C","G"]:
        filtering = filtering & (df_inf.ref == args.select_base)
        base_out = ".{}".format(args.select_base)
    else:
        base_out = ""

    out_list = []
    output_o = "{}{}.filtered.txt".format(output,base_out)
    # df_inf = df_inf.replace([np.inf, -np.inf], np.nan).dropna(axis=0)
    df_inf.loc[filtering,].to_csv(output_o, sep='\t', index=None, header=True)
    out_list.append(output_o)
    # log INFO

    logging.info("Finish Filtering at {}\n".format(", ".join(out_list)))

if __name__ == '__main__':
    parser = get_args()
    args = parser.parse_args()
    print("test filter")

    if args.input is None:
        print('\n*************************************************************')
        print('Eligos error: Must provide the Eligos result')
        print('\n*************************************************************\n')
        parser.print_help()
    else:
        _filter_main(args)
