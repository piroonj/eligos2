"""
.. module:: pair_diff_mod
   :platform: Unix, MacOSX
   :synopsis: Module for identifing RNA modification against control condition. 

.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com>

"""
#!/usr/bin/env python
import re, os, sys
import pysam
import pybedtools as bt
import pandas as pd
import numpy as np
import multiprocessing as mp
from concurrent import futures
import argparse
from tqdm import tqdm
import shutil
import json

import rpy2.robjects as robjects 
from rpy2.robjects import pandas2ri
pandas2ri.activate()
as_data_frame = robjects.r['as.data.frame']

from _misc import *
from _eligos_func import *
from time import sleep

def _pair_diff_mod_main(args):
    '''Identify RNA modification'''
    ## log INFO
    # if not args.quiet:
    logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    logging.info('Identify RNA modification against control condition')
    logging.info(str(args))

    threads = args.threads

    force = args.force

    modelF = args.model

    tBams = list(set(args.test_bams))
    cBams = list(set(args.ctrl_bams))

    bedF = args.region
    fafile = args.reference
    outpath = args.outdir
    tmpDir = args.sub_bam_dir
    bname = args.prefix

    bcfF = args.cdna_bcf

    if len(tBams) == 0 or bedF is None or fafile is None:
        logging.info("\nEligos require parameters: -i, -reg, and -ref to start the program\n")
        exit()

    ## check Output and Tmp directory
    outpath = ensure_dir(outpath, force, sleep=True)
    tmpDir = ensure_dir(os.path.join(outpath, tmpDir), force, sleep=True)

    ## check number of BCF files have to equal to number of 
    if len(bcfF) > 0 and len(bcfF) != len(tBams):
        logging.info("\nNumber of BCF file(s) have to equal to input BAM file(s).\n")
        exit()
    elif len(bcfF) == 0:
        bcfF = [False]*len(tBams)

    ## load rBEM5+2 model
    kmerGuppy = loadModel(modelF)

    filter_criteria = { 'max_depth': args.max_depth, 
                        'min_reads': args.min_depth, 
                        'oddR': args.oddR, 
                        'adjPval': args.adjPval,
                        'pval': args.pval, 
                        'errR': args.esb,
                        'modelF': kmerGuppy}

    ## prepare input data
    bamInfo = getBAMinfo(tBam=tBams, cBam=cBams, bcfF=bcfF)

    ## read and merge overlaping genes
    mergedBed = readBed(bedF)

    ## run split bam according to merged regions
    logging.info("Run build gene database")
    
    for e_bamInfo in bamInfo:
        tname = e_bamInfo['test']['bname']
        cname = e_bamInfo['ctrl']['bname']
        logging.info("\nRun ELIGOS comparing: {} vs {}".format(tname,cname))
        params = setParams([e_bamInfo], fafile, mergedBed, tmpDir, threads, filter_criteria) 
        sleep(1)

        logging.info("Run error extraction")
        (bedFbname, bedFext) = os.path.splitext(os.path.basename(bedF))
        bname = "{}_vs_{}_on_{}".format(tname, cname, bedFbname)
        extractModifications(params, bname, outpath, mergedBed, threads)

    logging.info("Finish run ELIGOS!")

if __name__ == '__main__':
    parser = get_args()
    args = parser.parse_args()
    print("test pair_diff_mod")

    if len(args.test_bams) == 0 or len(args.ctrl_bams) == 0  or args.region is None or args.reference is None:
        print('\n*********************************************************************************')
        print('Eligos error: Must provide Regions (BED6/BED12), test and control BAM and Reference sequence file')
        print('\n*********************************************************************************\n')
        save_args.append('-h')
        parser.parse_args(save_args)
        parser.print_help()
    else:
        _pair_diff_mod_main(args)

