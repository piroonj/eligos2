"""
.. module:: multi_samples_test
   :platform: Unix, MacOSX
   :synopsis: module for Filtering mapped reads from bam file

.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com> 

"""
#!/usr/bin/env python

import pandas as pd
import numpy as np
import rpy2.robjects as robjects 
from rpy2.robjects import pandas2ri
pandas2ri.activate()
# as_data_frame = robjects.r['as.data.frame']

from _misc import *
# from _eligos_func import *
from time import sleep

def get_args():
    example_text = '''Test filter:

    With output as File:
      %(prog)s filter -i file.txt 

     '''
    
    import _option_parsers
    parser = _option_parsers.get_filter_parser()

    return(parser)

def loadCMH_Multi(rep=None):
    # import rpy2.robjects as robjects 
    # from rpy2.robjects import pandas2ri
    # pandas2ri.activate()
    global as_data_frame 
    as_data_frame = robjects.r['as.data.frame']
    
    if rep > 1:
        robjects.r('''
        library(samplesizeCMH)
        #test_CMH
        rep_list <- paste0("rep", seq(1, {rep}))
        testStats <- function(dat){{
            Errors <- array(dat,
                        dim = c(2, 2, {rep}), # {rep} is number of replication
                        dimnames = list(
                            Sequence = c("dRNA", "dcDNA"),
                            Check = c("error", "correct"),
                            Replicate = rep_list))
            ##### c(misRNA,misDNA,corectRNA,corectDNA) ####
            CMHres = data.frame(p.value = NA,estimate = NA )
            try(CMHres <- mantelhaen.test(Errors, alternative = "t"),silent = T)
            stat = rbind(CMHres$estimate,CMHres$p.value)
            return(stat)
        }}

        out_stats <- function(data){{
            stat = apply(data, 1, (testStats))
            #adjust for multiple testing. 
            Stat = cbind(t(stat),p.adjust(stat[2,], method = 'BH') )
            colnames(Stat) = c('oddR', 'pval', 'adjPval')
            return(Stat)
        }}
                '''.format(rep=rep) )
    elif rep == 1:
        robjects.r('''
        library(samplesizeCMH)
        #test_fisher
        testStats <- function(dat){ 
            Errors <-   matrix(dat,2,2)    
            colnames(Errors) = c("dRNA", "dcDNA")
            rownames(Errors) = c("error", "correct")
            ##### c(misRNA,misDNA,corectRNA,corectDNA) ####
            Fisres = data.frame(p.value = NA,estimate = NA )
            try(Fisres <- fisher.test(Errors), silent = T)
            stat = rbind(Fisres$estimate,Fisres$p.value)
            return(stat)
        }

        out_stats <- function(data){
            stat = apply(data, 1, (testStats))
            #adjust for multiple testing. 
            Stat = cbind(t(stat),p.adjust(stat[2,], method = 'BH') )
            colnames(Stat) = c('oddR', 'pval', 'adjPval')
            return(Stat)
        }
                ''')

    global r_tCMH 
    r_tCMH = robjects.globalenv['testStats']
    global r_outStat 
    r_outStat = robjects.globalenv['out_stats']

def testCMH(data):
    r_data = pandas2ri.py2ri(data)
    stats_df = pandas2ri.ri2py(as_data_frame(r_outStat(r_data)))
    return stats_df

def sign_filt(data, idx):
    return [ data[i] for i in idx ]

def readMultiData(data):
    '''Combine multiple test or control samples into one table
    '''
    dataInfo=[]
    combineErr = pd.DataFrame()
    for idx, file in enumerate(data['ctrl']):
        oldcolnames = ["chrom","start_loc","end_loc","strand","name",'drna_err_1', 'model_err_1', 'drna_cor_1', 'model_cor_1']
        colnames = ["chrom","start_loc","end_loc","strand","name",'drna_err_{}'.format(idx+1),'model_err_{}'.format(idx+1),'drna_cor_{}'.format(idx+1),'model_cor_{}'.format(idx+1)]
        d = pd.read_table(file)
        d2 = d.loc[:,oldcolnames]
        d2.columns = colnames
        if len(combineErr.columns) == 0:
            combineErr = d2
        else:
            combineErr = pd.merge(combineErr, d2, on=["chrom","start_loc","end_loc","strand","name"])
        dataInfo.append(d.loc[:,['chrom','start_loc','end_loc','strand','name','ref', 'homo_seq','triplet','kmer7']])

    ## only keep common items between tables
    dataInfoNew = pd.concat(dataInfo, axis=0, join='inner').drop_duplicates()
    return dataInfoNew, combineErr

def readMultiDataPair(test, ctrl):
    '''Combine multiple test or control samples into one table
    '''
    combineErr = pd.DataFrame()
    dataInfo=[]
    for idx, (testF, ctrlF) in enumerate(zip(test,ctrl)):
        # print(testF, ctrlF)
        # oldcolnames = ["chrom","start_loc","end_loc","strand","name",'test_err_1', 'model_err_1', 'test_cor_1', 'model_cor_1']
        colnames = ["chrom","start_loc","end_loc","strand","name",'test_err_{}'.format(idx+1),'ctrl_err_{}'.format(idx+1),'test_cor_{}'.format(idx+1),'ctrl_cor_{}'.format(idx+1)]
        c = pd.read_table(ctrlF)
        t = pd.read_table(testF)
        oldcolnames = ["chrom","start_loc","end_loc","strand","name",'test_err_1', 'test_cor_1']
        d2 = pd.merge(t.loc[:,oldcolnames], c.loc[:,oldcolnames], on=["chrom","start_loc","end_loc","strand","name"], how="inner").dropna(axis = 0, how ='any')
        d2 = d2.iloc[:,[0,1,2,3,4,5,7,6,8]]
        d2.columns = colnames
        if len(combineErr.columns) == 0:
            combineErr = d2
        else:
            combineErr = pd.merge(combineErr, d2, on=["chrom","start_loc","end_loc","strand","name"])
        dataInfo.append(c.loc[:,['chrom','start_loc','end_loc','strand','name','ref', 'homo_seq','kmer5','kmer7']])
        dataInfo.append(t.loc[:,['chrom','start_loc','end_loc','strand','name','ref', 'homo_seq','kmer5','kmer7']])
    dataInfoNew = pd.concat(dataInfo, axis=0, join='inner').drop_duplicates()
    return dataInfoNew, combineErr

def _multi_samples_test_main(args):

    logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    logging.info('Perform multi-samples testing of rna_mod results')
    logging.info(str(args))

    ## Input data
    tname = args.test_mods
    cname = args.ctrl_mods
    oddR = args.oddR
    absLog2oddR = args.absLog2oddR
    adjPval = args.adjPval
    pval = args.pval
    homopolymer = args.homopolymer
    select_base = args.select_base

    outpath = ensure_dir(args.outdir)

    # output
    if args.prefix is None:
        output_o = "result.CMH_testing"
    else:
        output_o = f"{args.prefix}.CMH_testing"
        
    output_o = os.path.join(outpath, output_o)

    if len(tname) == len(cname):
        loadCMH_Multi(rep=len(tname))
    else:
        exit("The test and control samples are imbalance.")

    ## test CMH
    dataInfoTest, combineErrTest = readMultiDataPair(test=tname, ctrl=cname)
    paired_test_df = testCMH(combineErrTest.iloc[:,5:])

    ## concatenate info with new test
    tmp = pd.concat([combineErrTest.reset_index(drop=True), paired_test_df.reset_index(drop=True)], axis=1)
    result_treat_ctrl = pd.merge(dataInfoTest, tmp, on=["chrom","start_loc","end_loc","strand","name"], how="inner").dropna(axis = 0, how ='any')
    result_treat_ctrl['log2OddR'] = np.log2(result_treat_ctrl.oddR)

    filterCriteria = f'oddR>{oddR} & abs(log2OddR)>{absLog2oddR} & adjPval<{adjPval} & pval<{pval}'
    if homopolymer:
        filterCriteria += ' & homo_seq == "--"'
    if select_base in ["A","T","C","G"]:
        filterCriteria += f' & ref == "{select_base}"'
        output_o = f"{output_o}.{select_base}"

    output_o = f"{output_o}.txt"

    # print(filterCriteria)
    result_treat_ctrl_final = result_treat_ctrl.query(filterCriteria)
    result_treat_ctrl_final.to_csv(output_o, sep='\t', index=None, header=True)
    # print(output_o)


    # log INFO
    logging.info("Finish run Multiple samples testing!")

if __name__ == '__main__':
    parser = get_args()
    args = parser.parse_args()
    print("test multi_samples_test")

    if len(args.test_mods) != len(args.ctrl_mods) or len(args.test_mods) <= 1:
        print('\n*********************************************************************************')
        print('Eligos error: Must provide multiple rna_mod results with ')
        print('the same number of samples between test and control.')
        print('\n*********************************************************************************\n')
        parser.print_help()
    else:
        _multi_samples_test_main(args)



