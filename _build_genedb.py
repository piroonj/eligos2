"""
.. module:: build_genedb
   :platform: Unix, MacOSX
   :synopsis: Module for preparing BAM files according to gene regions. 
              As an option for accelerating the process in huge genome like human, mouse, etc.  

.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com>

"""
#!/usr/bin/env python
import argparse
import pysam
from tqdm import tqdm
import os.path, sys
from concurrent import futures
import logging
from _misc import *
from _eligos_func import readBed
from time import sleep
from filecmp import cmp

## Data preparation functions ######################

def get_args():
    example_text = '''Test build_genedb:

    With output in directory:
      %(prog)s build_genedb -o results -regions file.bed file.bam

     '''
    import _option_parsers
    parser = _option_parsers.get_build_genedb_parser()

    return(parser)


def setParamsBuildGeneDB(bamsF=[], mergedBed=None, tmpDir=None):
    subset_bam_params_pos = [] 
    subset_bam_params_neg = [] 
    print("\nbamInfo:")
    print(bamsF)
    print("\nPrepare search regions:")    
    
    for bamF in bamsF:
        with pysam.AlignmentFile(bamF, "rb") as bamObj:
            if not bamObj.has_index():
                exit("{} have no index file (.bai)\nPlease index BAM file using samtools index\n".format(bamF))
            chroms = [ b.contig for b in bamObj.get_index_statistics()]

        print("Prepare directory for {}".format(bamF))
        with tqdm(total=len(mergedBed), desc="Progress ") as pbar: 
            for idx, row in enumerate(mergedBed):
                # chrom= row['chrom']
                # start= row['start']
                # end  = row['end'] 
                # strand = True if row['strand'] == '-' else False
                # strand_label = "pos" if row['strand'] == '-' else "neg"
                chrom= row[0]
                start= int(row[1])
                end  = int(row[2])
                rv_strand = True if row[3] == '-' else False
                strand_label = "pos" if row[3] == '+' else "neg"
                if chrom not in chroms:
                    continue
                tmpPath = os.path.join(tmpDir,bamF,chrom,"{:0>3}".format(str(start)[:3]))
                tmpPath = ensure_dir(tmpPath)
                bamFile = os.path.join(tmpPath, "{}_{}_{}_{}.bam".format(chrom,start,end,strand_label))

                if rv_strand:
                    subset_bam_params_neg.append((bamF, chrom, start, end, bamFile, strand_label))
                else:
                    subset_bam_params_pos.append((bamF, chrom, start, end, bamFile, strand_label))
                pbar.update()

    print("\nSubset BAM according to gene regions:")
    print("Pos:",len(subset_bam_params_pos))
    if len(subset_bam_params_pos) > 0:
        with futures.ProcessPoolExecutor(max_workers=1) as executor:
            with tqdm(total=len(subset_bam_params_pos), desc="Subset + strand Progress ") as pbar:
                for _ in executor.map(subset_bam_thread, subset_bam_params_pos):
                    pbar.update()
    print("Neg:",len(subset_bam_params_neg))
    if len(subset_bam_params_neg) > 0:
        with futures.ProcessPoolExecutor(max_workers=1) as executor:
            with tqdm(total=len(subset_bam_params_neg), desc="Subset - strand Progress ") as pbar:
                for _ in executor.map(subset_bam_thread, subset_bam_params_neg):
                    pbar.update()


def _build_genedb_main(args):
    '''Build bam from gene database'''

    outpath = args.outdir
    bamsF = list(set(args.bam))
    bedF = args.region
    tmpDir = args.sub_bam_dir
    force = args.force

    ## log INFO
    # if not args.quiet:
    logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    logging.info('Build bam from gene database')
    logging.info(str(args))

    outpath = ensure_dir(outpath, force, sleep=True)
    tmpDir = ensure_dir(os.path.join(outpath, tmpDir), force, sleep=True)
    
    ## read and merge overlaping genes
    mergedBed = readBed(bedF)

    ## run split bam according to merged regions
    subset_bam_params = setParamsBuildGeneDB(bamsF, mergedBed, tmpDir)

    logging.info("\nFinish build_genedb\n")

if __name__ == '__main__':
    parser = get_args()
    args = parser.parse_args()
    print("test build gene database")

    if args.bam is None or args.region is None:
        print('\n*************************************************************')
        print('Eligos error: Must provide Regions (BED) or BAM file')
        print('\n*************************************************************\n')
        parser.print_help()
    else:
        _build_genedb_main(args)
