"""
.. module:: eligos_func
   :platform: Unix, MacOSX
   :synopsis: support functions for Eligos

.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com>

"""
import re, os, sys
import pysam
import pybedtools as bt
import pandas as pd
import numpy as np
import multiprocessing as mp
from concurrent import futures
import argparse
from tqdm import tqdm
import shutil
import json
from _misc import *

import rpy2.robjects as robjects 
from rpy2.robjects import pandas2ri
pandas2ri.activate()
as_data_frame = robjects.r['as.data.frame']

##---------------------------------------
##### Base management
def complement(base):
    base_complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
    base = base.upper()
    base_out = ''.join([ base_complement.get(f, "N") for f in base])
    return base_out

def reversecomplement(base):
    base_complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
    base = base.upper()
    if "-" in base:
        return base
    else:
        base_out = "".join([base_complement.get(x, "N") for x in base][::-1])
    return base_out

##---------------------------------------
##### Decision modules
def rvStrand(reverse, is_reverse, bam_type=None):
    if bam_type == 'cdna':
        return True
    elif   reverse == True  and is_reverse:
        return True
    elif reverse == False and not is_reverse:
        return True
    else:
        return False

def featuretype_filter(feature, featuretype):
    if feature[2] == featuretype:
        return True
    return False

##---------------------------------------
##### Extract BAM parallel

def gen_params(fol_path, args):
    # input
    rna_bam=args.bam

    with pysam.AlignmentFile(rna_bam, 'rb') as inf:
        list_chr = inf.references

    list_path_args = [("{}/{}.bam".format(fol_path, x), args) for x in list_chr]

    return zip(list_chr, list_path_args), list_chr

def split_PySam_filter_by_chromosome(info):
    chromosome, tup_params = info
    output_path = tup_params[0]
    args = tup_params[1]
    total = 0
    filReads = 0
    # input
    rna_bam=args.bam
    # criteria
    mapQ = args.mapQ
    aln_length = args.aln_length
    percent_identity = args.percent_identity
    allow_supplementary = args.supplementary
    query_coverage = args.query_coverage

    with pysam.AlignmentFile(rna_bam) as inf:
        with pysam.AlignmentFile(output_path, "wb", template=inf) as outf:
            for read in inf.fetch(reference=chromosome, multiple_iterators=True):
                ## no unmapped and secondary
                if not read.is_unmapped and not read.is_secondary:
                    ## to minimize chrimeric reads
                    if read.is_supplementary:
                        if not allow_supplementary:
                            continue
                    total += 1
                    if read.mapping_quality>=mapQ and get_pID(read)>=percent_identity and get_coverage(read)>=query_coverage and read.query_alignment_length>=aln_length:
                        outf.write(read)
                        filReads += 1
    return (total, filReads)

def merge_sort_reads(bam_dir, out_path):
    bam_list = ["{}/{}".format(bam_dir, file_name) for file_name in os.listdir(bam_dir) if '.bam' in file_name]
    bam_merge_path = "{}/merged.bam".format(bam_dir)
    
    with pysam.AlignmentFile(bam_list[0], 'rb') as inf:
        with pysam.AlignmentFile(bam_merge_path, 'wb', template=inf) as outf:
            with tqdm(total=len(bam_list), desc="Merging chromosomes ") as pbar:
                for bam_path in bam_list:
                    with pysam.AlignmentFile(bam_path, 'rb') as inner_inf:
                        for read in inner_inf:
                            outf.write(read)
                    pbar.update()
    print('Combining result ...')
    pysam.sort('-o', out_path, bam_merge_path)

##---------------------------------------
##### Extract BAM information
## calculate percent identity
## calculate query length
## calculate mapped read coverage

def filter_reads(reads, min_mapq=0):
    reads = [read for read in reads if not read.alignment.is_supplementary and not read.alignment.is_unmapped and not read.alignment.is_duplicate and not read.is_refskip]
    if min_mapq > 0:
        reads = [read for read in reads if read.alignment.mapq >= min_mapq]
    return reads

def divided(x, y):
    if x == 0 or y == 0:
        return float(0)
    else:
        return float(x)/float(y)

def extractCIGAR(cigarstring, gap_compress=False):
    """Return CIGAR tag counts with or without gap-compressed"""
    cigar_pat = re.compile(r"(\d+)([MIDNSHP=X])")
    cigarextract = dict()
    for num, tag in cigar_pat.findall(cigarstring):
        if gap_compress and tag in ["D","I"]:
            cigarextract[tag] = cigarextract.setdefault(tag, 0) + int(1)
        else:
            cigarextract[tag] = cigarextract.setdefault(tag, 0) + int(num)
    return(cigarextract)
        
def queryLength(read, noClip=False):
    """Return the query read length based on CIGAR string"""
    cigar = extractCIGAR(read.cigarstring)
    if noClip:
        return sum([cigar[k] for k in cigar if k not in ['D','N','H','S'] ])
    else:
        return sum([cigar[k] for k in cigar if k not in ['D','N'] ])

def get_pID(read):
    """Return the percent identity of a read.
    based on the de tag if present,
    if not calculate from CIGAR with gap-compressed 
    """
    if read.has_tag("de"):
        return float("{:.3f}".format(100 * (float(1) - read.get_tag("de"))))
    elif read.has_tag("dv"):
        return float("{:.3f}".format(100 * (float(1) - read.get_tag("dv"))))
    else:
        if not re.search("(=|X)", read.cigarstring):
            exit("BAM file should have tag 'de' or 'dv' otherwise CIGAR should contain =/X CIGAR operators using Minimap2 with --eqx ")
        cigarCountDict = extractCIGAR(read.cigarstring, gap_compress=True)
        misMatchs = cigarCountDict.get('D',0)+cigarCountDict.get('I',0)+cigarCountDict.get('X',0)
        if misMatchs == 0:
            return float(100)
        else:
            return float("{:.3f}".format(100 * (1-(misMatchs/(cigarCountDict.get('=',0)+cigarCountDict.get('X',0))))))

def get_coverage(read):
    """Calculate aligned read coverage."""
    cigar_count = extractCIGAR(read.cigarstring)
    return float("{:.3f}".format(100 * ((read.query_alignment_length)/float(queryLength(read)))))


## Eligos mod functions

def getMajorAllele(chrom=None, start=None, end=None, bcf=None, qual=15, minDepth=20):
    for rec in bcf.fetch(chrom, start, end):
        if sum(rec.info["DP4"]) < minDepth or rec.qual < qual:
            return False
        refCount = sum(rec.info["DP4"][:2])
        altCount = sum(rec.info["DP4"][2:])
        refFreq = divided(refCount,float(refCount+altCount))
        altFreq = divided(altCount,float(refCount+altCount))
#         print(refFreq, altFreq)
        if refFreq > altFreq:
            return rec.ref
        else:
            return rec.alts[0]
    return False

def getBAMinfo(tBam=[], cBam=[], bcfF=[]):
    bamInfo = []
    if len(tBam) > 0 and len(cBam) > 0:
        if len(tBam) == len(cBam):
            for i in range(0,len(tBam)):
                tTmp = tBam[i]
                cTmp = cBam[i]
                bcfTmp = bcfF[i]
                bamInfo.append({'test' : {'bam': tTmp, 'bname': os.path.splitext(os.path.basename(tTmp))[0]},
                                'ctrl': {'bam': cTmp, 'bname': os.path.splitext(os.path.basename(cTmp))[0]},
                                'bcfF' : {'file': bcfTmp}})
            loadCMH(rep=1)
        else:
            cBamOrg = cBam
            cBam = [cBamOrg[0]]*len(tBam)
            ## replace orginal to generated control
            for i in range(0,len(cBamOrg)):
                cBam[i] = cBamOrg[i]
            for i in range(0,len(tBam)):
                tTmp = tBam[i]
                cTmp = cBam[i]
                bcfTmp = bcfF[i]
                bamInfo.append({'test' : {'bam': tTmp, 'bname': os.path.splitext(os.path.basename(tTmp))[0]},
                                'ctrl': {'bam': cTmp, 'bname': os.path.splitext(os.path.basename(cTmp))[0]},
                                'bcfF' : {'file': bcfTmp}})
            loadCMH(rep=1)

    elif len(tBam) > 0 and len(cBam) == 0:
        ## get model test
        for i, tTmp in enumerate(tBam):
            bcfTmp = bcfF[i]
            bamInfo.append({'test' : {'bam': tTmp, 'bname': os.path.splitext(os.path.basename(tTmp))[0]},
                            'ctrl': {'bam': False, 'model': 'model', 'bname': 'model'},
                            'bcfF' : {'file': bcfTmp}})
        loadCMH(rep=1)
    else:
        exit("Unbalance of paired samples")
    return bamInfo

def loadModel(modelF):
    if modelF:
        with open(modelF, 'r') as j:
            kmerGuppy = eval(json.load(j))
    else:
        modelsF = os.path.join(os.path.dirname(__file__), 'models','eligos_dRNA_ivt_model.v1.0.json')
        with open(modelsF, 'r') as j:
            kmerGuppy = eval(json.load(j))
    return kmerGuppy

def outputIntial(beds):
    out_data = {0: [""]*len(beds), 1: [""]*len(beds), 2: [""]*len(beds),
               'combine': [""]*len(beds)}
    return out_data

def searchUnamibuous(seq):
    genericDNA = set(list("ATCG"))
    count = len(set(list(seq))-genericDNA)
    return(count)

## Data preparation functions ######################
def setParams(bamInfo=None, fafile=None, mergedBed=None, tmpDir=None, threads=None, filter_criteria=None):
    params = []
    subset_bam_params_pos = [] 
    subset_bam_params_neg = [] 
    faidx = pysam.FastaFile(fafile)
    print("bamInfo:")
    print(bamInfo)
    print("\nPrepare search regions:")

    chroms = set([])
    for i in range(0,len(bamInfo)):
        for cond in ['test','ctrl']:
            bam = bamInfo[i][cond]
            if bam['bam']:
                with pysam.AlignmentFile(bam['bam'], "rb") as bamObj:
                    if not bamObj.has_index():
                        exit("{} have no index file (.bai)\nPlease index BAM file using samtools index\n".format(bamF))
                    for b in bamObj.get_index_statistics():
                        chroms.add(b.contig)

    with tqdm(total=len(mergedBed), desc="Prepare Progress ") as pbar:
        for index, row in enumerate(mergedBed):
            chrom= row[0]
            start= int(row[1])
            end  = int(row[2])
            rv_strand = True if row[3] == '-' else False
            strand_label = "pos" if row[3] == '+' else "neg"
            name = row[4]
            if chrom not in chroms:
                continue
            subSeq = faidx.fetch(chrom, start, end).upper()
            bamFiles = []
            for i in range(0,len(bamInfo)):
                bamTmp = {}

                for cond in ['test','ctrl']:
                    bam = bamInfo[i][cond]
                    if bam['bam']:
                        tmpPath = ensure_dir(os.path.join(tmpDir, "{}".format(bam['bname'])))
                        bamFile = os.path.join(tmpPath,chrom,"{:0>3}".format(str(start)[:3]), "{}_{}_{}_{}.bam".format(chrom,start,end,strand_label))
                        if rv_strand:
                            subset_bam_params_neg.append((bam['bam'], chrom, start, end, bamFile, strand_label))
                        else:
                            subset_bam_params_pos.append((bam['bam'], chrom, start, end, bamFile, strand_label))
                        bamTmp.setdefault(cond, bamFile)
                        bamTmp['bcfF'] = bamInfo[i]['bcfF']['file']
                    else:
                        bamTmp.setdefault(cond, 'model')
                        bamTmp['bcfF'] = bamInfo[i]['bcfF']['file']
                bamFiles.append(bamTmp)
            params.append((index, bamFiles, subSeq, chrom, start, end, name, rv_strand, filter_criteria))
            pbar.update(1)
    faidx.close()
#     print(subset_bam_params)
#     print("--------------------------\n")
#     print(params)

    if len(subset_bam_params_pos) > 0:
        with futures.ProcessPoolExecutor(max_workers=1) as executor:
            with tqdm(total=len(subset_bam_params_pos), desc="Subset + strand Progress ") as pbar:
                for _ in executor.map(subset_bam_thread, subset_bam_params_pos):
                    pbar.update(1)

    if len(subset_bam_params_neg) > 0:
        with futures.ProcessPoolExecutor(max_workers=1) as executor:
            with tqdm(total=len(subset_bam_params_neg), desc="Subset - strand Progress ") as pbar:
                for _ in executor.map(subset_bam_thread, subset_bam_params_neg):
                    pbar.update(1)

    print("Finish build_genedb\n")

    return params

## Error extraction functions ######################
def extract_error(bam=None, bam_type=None, faidx=None, chrom=None, start=None, end=None, 
                  name=None, reverse=None, max_depth=100000, bcf=False):
    outTmp = [['-',0,0,'-','-','-',0,0,0,0,0,0,0,0,0,0,'-','-','-',0,'-','-','-']]*(end-start)
    if not os.path.isfile(bam):
        return outTmp

    homopolymer = re.compile(r'(A{4,}|T{4,}|C{4,}|G{4,})', re.I)

    bamData = pysam.AlignmentFile(bam, "rb")
    # print("in:",chrom, start, end, bam)
    for pileupcolumn in bamData.pileup(chrom, start, end, truncate=True, max_depth=max_depth):
        reads = filter_reads(pileupcolumn.pileups)
        # reads_nodel = [read for read in reads if not read.is_del]
        start_b = pileupcolumn.pos - start
        end_b = start_b + 1
        start_loc = start + start_b 
        end_loc = start_loc + 1
        ref = faidx[start_b:end_b].upper()
        kmer5 = faidx[start_b-2:end_b+2].upper()
        kmer7 = faidx[start_b-2:end_b+4].upper()
        strand = '-' if reverse else '+'
        
        reads_o = len( [read for read in reads 
                        if rvStrand(reverse, read.alignment.is_reverse, bam_type)] )
        matches_o, substitutions_o, substitutions_wo_ins_o = (0,0,0)
        basesCounts = {"A":0,"T":0,"C":0,"G":0}
        
#         print("in:",name, start_b, end_b, start_loc, end_loc, ref, strand, reads_o)
        
        cdnaMajorAllele = False
        if bcf:
            ## TODO: need to verify INDEL possibility for filtering out INDEL cDNA from analysis
            cdnaMajorAllele = getMajorAllele(chrom, pileupcolumn.pos, pileupcolumn.pos+1, bcf)

        for read in reads:
            ## count matches, substitutions
            if not read.is_del:
                if rvStrand(reverse, read.alignment.is_reverse, bam_type):
                    basesCounts[read.alignment.seq[read.query_position]] += 1
                    if cdnaMajorAllele:
                        if read.alignment.seq[read.query_position] == cdnaMajorAllele:
                            matches_o += 1
                        else:
                            substitutions_o += 1
                            if not read.indel > 0:
                                substitutions_wo_ins_o += 1
                    else:
                        if read.alignment.seq[read.query_position] == ref:
                            matches_o += 1
                        else:
                            substitutions_o += 1
                            if not read.indel > 0:
                                substitutions_wo_ins_o += 1

        ## Major allel
        majAllel = max(basesCounts, key=lambda k: basesCounts[k])
        majAllelFreq = divided(basesCounts[majAllel],float(sum(basesCounts.values())))
        ## homopolymer count
        homoseq = "--"
        test_start_home = start_b-2 if start_b-2 > 0 else 0
        homo = homopolymer.search(faidx[test_start_home:start_b+4])
        if(homo):
            homoseq = homo.group(1).upper()
        # print(chrom,start_loc,end_loc, ref, faidx[test_start_home:start_b+4], homoseq)
        deletions_o = len( [read for read in reads
                        if read.is_del and rvStrand(reverse, read.alignment.is_reverse, bam_type)] )
        insertions_o = len( [read for read in reads 
                          if read.indel > 0 and rvStrand(reverse, read.alignment.is_reverse, bam_type)] )
        error_o = substitutions_wo_ins_o + insertions_o + deletions_o
        o_ref = complement(ref) if reverse else ref
        o_kmer5 = reversecomplement(kmer5) if reverse else kmer5
        o_homoseq = reversecomplement(homoseq) if reverse else homoseq
        o_kmer7 = reversecomplement(kmer7) if reverse else kmer7
        o_majAllel = reversecomplement(majAllel) if reverse else majAllel
        ## for model matching 
        m_kmer5 = kmer5
        m_kmer7 = kmer7
        outTmp[start_b] = [chrom,start_loc,end_loc,strand ,name, o_ref, 
                            reads_o, matches_o, error_o,
                            substitutions_o, deletions_o, insertions_o,  
                            divided(error_o, reads_o),
                            divided(substitutions_o, reads_o),
                            divided(deletions_o, reads_o),
                            divided(insertions_o, reads_o), 
                            o_homoseq, o_kmer5, o_majAllel, majAllelFreq, o_kmer7, 
                            m_kmer5, m_kmer7
                           ]
#         print(outTmp[start_b])
    bamData.close()
    return outTmp

def extractModifications_thread(param):
    order = int(param[0])
    bamFiles = param[1]
    subSeq = param[2]
    chrom = param[3]
    start = int(param[4])
    end = int(param[5])
    name = param[6]
    reverse = param[7]

    strand = "-" if reverse else "+"
    max_depth = param[8]['max_depth']
    min_reads = param[8]['min_reads']
    oddR = param[8]['oddR']
    adjPval = param[8]['adjPval']
    pval = param[8]['pval']
    errR = param[8]['errR']
    kmerGuppy = param[8]['modelF']

    ## header for error_extract
    header = ["chrom","start_loc","end_loc","strand","name","ref","read","matches","err",
          "sub","del","ins","r_err","r_sub","r_del","r_ins",'homo_seq','kmer5',
          'majorAllel','majorAllelFreq','kmer7','m_kmer5','m_kmer7']
    ## header for output
    header_out = ["chrom","start_loc","end_loc","strand","name","ref",
             'homo_seq','kmer5','majorAllel','majorAllelFreq','kmer7']

#         print(bamFiles)
    data_for_test = {}
    out = {'order': order, 'result':{0:pd.DataFrame([]), 1:pd.DataFrame([]), 2:pd.DataFrame([]), 'combine':pd.DataFrame([])} }

    for bam_idx in range(0, len(bamFiles)):
        err_data = {}
        index = []

#         print(bam_idx, bamFiles[bam_idx])
        if bamFiles[bam_idx]['bcfF']:
            bcf = pysam.VariantFile(bamFiles[bam_idx]['bcfF'])
        else:
            bcf = False
        
        checkPairError = True
        test_err = extract_error(bamFiles[bam_idx]['test'], 'drna', subSeq, chrom, start, end, name, reverse, max_depth, bcf)        
        test_err_df = pd.DataFrame(test_err, columns=header)
        # test_err_df = test_err_df.loc[test_err_df['read']>=min_reads,]
        if test_err_df.shape[0] == 0:
            checkPairError = False

        if bamFiles[bam_idx]['ctrl'] != 'model':
            ## TODO: need to verify INDEL possibility for filtering out INDEL cDNA from analysis
            ctrl_err = extract_error(bamFiles[bam_idx]['ctrl'], 'drna', subSeq, chrom, start, end, name, reverse, max_depth, bcf)
            ctrl_err_df = pd.DataFrame(ctrl_err, columns=header)
            # ctrl_err_df = ctrl_err_df.loc[ctrl_err_df['read']>=min_reads,]
            if ctrl_err_df.shape[0] == 0:
                checkPairError = False
            else:
                filterMatch = list(set(test_err_df.index)&set(ctrl_err_df.index))
                filterMatch = list(map(int, filterMatch)) 
                test_err_df = test_err_df.loc[filterMatch,]
                if test_err_df.shape[0] == 0:
                    checkPairError = False
            ctrl_lab = "ctrl"
        else:
            ctrl_lab = "model"

        if checkPairError:
            for idx, row in test_err_df.iterrows():
                if (ctrl_lab == "model" and len(row.kmer5) == 5 and searchUnamibuous(row.kmer5) == 0) or (ctrl_lab == "ctrl"):
                    err_pass = False
                    err_data_tmp = {}
                    for baseExt in [0,1,2]:
                        sub_test_err_df = test_err_df.iloc[idx-baseExt:idx+baseExt+1]
                        test_nerr = sub_test_err_df.err.sum()
                        test_total = sub_test_err_df.read.sum()
                        test_matchNsub = sub_test_err_df["matches"].sum() + sub_test_err_df["sub"].sum()
                        test_ncor =  test_total-test_nerr
                        test_err_r = divided(test_nerr, test_total)
                        if ctrl_lab == "model":
                            if row.m_kmer7 in kmerGuppy['k7'] and (kmerGuppy['k7'][row.m_kmer7][baseExt]['nerr']!=0 and kmerGuppy['k7'][row.m_kmer7][baseExt]['ncor']!=0):
                                                            ##|- - to avoid odd = inf - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
                                ctrl_nerr = kmerGuppy['k7'][row.m_kmer7][baseExt]['nerr']
                                ctrl_ncor = kmerGuppy['k7'][row.m_kmer7][baseExt]['ncor']
                                ctrl_total = kmerGuppy['k7'][row.m_kmer7][baseExt]['ntotal']
                                ctrl_matchNsub = kmerGuppy['k7'][row.m_kmer7][baseExt]['ntotal'] - kmerGuppy['k7'][row.m_kmer7][baseExt]['nerr'] + kmerGuppy['k7'][row.m_kmer7][baseExt]['nsub']
                            else:
                                ctrl_nerr = kmerGuppy['k5'][row.m_kmer5][baseExt]['nerr']
                                ctrl_ncor = kmerGuppy['k5'][row.m_kmer5][baseExt]['ncor']
                                ctrl_total = kmerGuppy['k5'][row.m_kmer5][baseExt]['ntotal']
                                ctrl_matchNsub = kmerGuppy['k5'][row.m_kmer5][baseExt]['ntotal'] - kmerGuppy['k5'][row.m_kmer5][baseExt]['nerr'] + kmerGuppy['k5'][row.m_kmer5][baseExt]['nsub']
                            err_data_tmp.setdefault(baseExt, []).append([test_nerr, ctrl_nerr, test_ncor, ctrl_ncor])
                        elif ctrl_lab == "ctrl":
                            sub_ctrl_err_df = ctrl_err_df.iloc[idx-baseExt:idx+baseExt+1]
                            ctrl_nerr = sub_ctrl_err_df.err.sum()
                            ctrl_total = sub_ctrl_err_df.read.sum()
                            ctrl_matchNsub = sub_ctrl_err_df["matches"].sum() + sub_ctrl_err_df["sub"].sum()
                            ctrl_ncor =  ctrl_total-ctrl_nerr
                            err_data_tmp.setdefault(baseExt, []).append([test_nerr, ctrl_nerr, test_ncor, ctrl_ncor])
                        else:
                            exit("ctrl_lab error")

                        if test_matchNsub >= min_reads and ctrl_matchNsub >= min_reads and test_err_r >= errR and test_nerr > 0 and baseExt == 0:
                            err_pass = True

                    if err_pass:
                        for baseExt in [0,1,2]:
                            err_data.setdefault(baseExt, []).append(err_data_tmp[baseExt][0])
                        index.append(str(idx))
    #             print(len(index))
            if len(index) > 0:
                for baseExt in [0,1,2]:
                    data_for_test.setdefault(baseExt, []).append(pd.DataFrame(err_data.get(baseExt, []), columns=['test_err_{}'.format(bam_idx+1),'{}_err_{}'.format(ctrl_lab,bam_idx+1),'test_cor_{}'.format(bam_idx+1),'{}_cor_{}'.format(ctrl_lab,bam_idx+1)], index=index))

    stats_df = {}
    combineErr = {}
    stats_pass = set()
    for baseExt in [0,1,2]:
        if len(data_for_test.get(baseExt, [])) > 0:
            combineErr[baseExt] = pd.concat(data_for_test[baseExt], axis=1, join='inner')
            try:
                stats_df[baseExt] = testCMH(combineErr[baseExt])
            except:
                print(combineErr[baseExt])
                combineErr[baseExt].to_csv("error_test.txt",sep="\t",index=False)
                exit("error testCMH")
    #             print(len(set(stats_df[baseExt].loc[(stats_df[baseExt].oddR >= oddR)&(stats_df[baseExt].adjPval <= adjPval),].index)))
            stats_pass = stats_pass | set(stats_df[baseExt].loc[(stats_df[baseExt].oddR >= oddR)&((stats_df[baseExt].adjPval <= adjPval) & (stats_df[baseExt].pval <= pval)),].index)

    test_err_df.index = list(map(str,test_err_df.index))
    stats_pass = list(stats_pass)
    stats_pass.sort(key = int)
    if len(stats_pass) > 0:
        result_combine = []
        for baseExt in [0,1,2]:
            result_each = []
            combineErr[baseExt].index = list(map(str,combineErr[baseExt].index))
            stats_df[baseExt].index = list(map(str,stats_df[baseExt].index))
            result_each.append(pd.concat([test_err_df.loc[stats_pass,header_out], combineErr[baseExt].loc[stats_pass,], stats_df[baseExt].loc[stats_pass,]], axis=1, join='inner'))
            result_each = pd.concat(result_each)
            result_each['baseExt'] = baseExt
            result_each['total_reads'] = result_each['test_err_1'] + result_each['test_cor_1']
            result_each['ESB_test'] = result_each['test_err_1'] / result_each['total_reads']
            result_each['ESB_ctrl'] = result_each['{}_err_1'.format(ctrl_lab)] / (result_each['{}_err_1'.format(ctrl_lab)]+result_each['{}_cor_1'.format(ctrl_lab)])

            # maxOddR = round(max(result_each.loc[np.isfinite(result_each['oddR']), 'oddR']))
            # result_each['oddR'] = result_each['oddR'].replace(np.inf, maxOddR)
    #         result_each.to_csv("{}_test_vs_{}_baseExt{}.txt".format(bname,ctrl_lab,baseExt),sep="\t",index=None)
    #         print(result_each.shape)
            out['result'][baseExt] = result_each
            result_combine.append(result_each)

        combineTable = []
        for iCmb in range(0,len(result_combine[0])):
            row = pd.concat([result_combine[0].iloc[[iCmb]], result_combine[1].iloc[[iCmb]], result_combine[2].iloc[[iCmb]]])
            combineTable.append(row[(row.adjPval <= adjPval)&(row.pval <= pval)].nlargest(1, 'oddR', keep='last'))
        combineTable = pd.concat(combineTable)
    #     combineTable.to_csv("{}_test_vs_{}_combine.txt".format(bname,ctrl_lab),sep="\t",index=None)
    #     print(result_model.shape)
        out['result']['combine'] = combineTable
    
    return out

def extractModifications(params, bname, outpath, mergedBed, threads):
    result = outputIntial(mergedBed)

    # with mp.Pool(processes=threads) as pool:
    #     with tqdm(total=len(params), desc="Progress ") as pbar:
    #         for out in pool.imap_unordered(extractModifications_thread, params):
    #             result[0][out['order']] = out['result'][0]
    #             result[1][out['order']] = out['result'][1]
    #             result[2][out['order']] = out['result'][2]
    #             result['combine'][out['order']] = out['result']['combine']
    #             pbar.update()

    with futures.ProcessPoolExecutor(max_workers=threads) as executor:
        with tqdm(total=len(params), desc="Progress ") as pbar:
            for out in executor.map(extractModifications_thread, params):
                result[0][out['order']] = out['result'][0]
                result[1][out['order']] = out['result'][1]
                result[2][out['order']] = out['result'][2]
                result['combine'][out['order']] = out['result']['combine']
                pbar.update()    

    for k in [0,1,2,'combine']:
        result[k] = pd.concat(result[k])
        if len(result[k]) > 0:
            result[k] = result[k].loc[(result[k]['test_err_1']>0)&(result[k]['total_reads'] >= params[0][8]['min_reads'])&(result[k]['ESB_test'] >= params[0][8]['errR'])&(result[k]['oddR'] >= params[0][8]['oddR'])&((result[k]['adjPval'] <= params[0][8]['adjPval'])&(result[k]['pval'] <= params[0][8]['pval'])),]
        if k == 'combine':
            result[k].to_csv(os.path.join(outpath,"{}_{}.txt".format(bname,k)),sep="\t",index=None)
        else:
            result[k].to_csv(os.path.join(outpath,"{}_baseExt{}.txt".format(bname,k)),sep="\t",index=None)


## Statistics functions ######################
def loadCMH(rep=None):
    '''Load R objects and activate python & R crosstalk'''
    import rpy2.robjects as robjects 
    from rpy2.robjects import pandas2ri
    pandas2ri.activate()
    global as_data_frame 
    as_data_frame = robjects.r['as.data.frame']
    
    
    robjects.r('''
    #test_fisher
    testStats <- function(dat){ 
        Errors <-   matrix(dat,2,2)    
        colnames(Errors) = c("test", "control")
        rownames(Errors) = c("error", "correct")
        ##### c(misTest,misCtrl,corectTest,corectCtrl) ####
        Fisres = data.frame(p.value = NA,estimate = NA )
         try(Fisres <- fisher.test(Errors), silent = T)
        stat = rbind(Fisres$estimate,Fisres$p.value)
        return(stat)
    }

    out_stats <- function(data){
        stat = apply(data, 1, (testStats))
        #adjust for multiple testing. 
        Stat = cbind(t(stat),p.adjust(stat[2,], method = 'BH') )
        colnames(Stat) = c('oddR', 'pval', 'adjPval')
        return(Stat)
    }
            ''')    

    global r_tCMH 
    r_tCMH = robjects.globalenv['testStats']
    global r_outStat 
    r_outStat = robjects.globalenv['out_stats']

def testCMH(data):
    '''Perform Fisher test'''
    r_data = pandas2ri.py2ri(data)
    stats_df = pandas2ri.ri2py(as_data_frame(r_outStat(r_data)))
    return stats_df

