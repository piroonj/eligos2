import sys
import argparse
import logging

if sys.version_info[0] > 2:
    unicode = str

##################################
###### Positional arguments ######
##################################

sub_bam_dir_opt=('--sub_bam_dir', {
    'type':str,
    'help':'Temporary directory path for storing subset of bam files (default: %(default)s)',
    'default':"tmp"})
output_bam_opt=('--output_bam', {
    'type':unicode,
    'help':'Output bam files (default: %(default)s)',
    'default':None})
rna_bam_opt=("rna_bam",{'type':unicode,'default':None,
    'help':"Direct RNA data in a sorted bam file"})
bam_opt=("--bam",{'type':unicode,'default':None,
    'help':"Sorted bam file"})
test_bam_opt=("test_bam",{'type':unicode,'default':None,
    'help':"Test RNA/DNA data in a sorted bam file"})
control_bam_opt=("ctrl_bam",{'type':unicode,'default':None,
    'help':"Control RNA/DNA data in a sorted bam file"})
eligos_result_opt=("--input",{'type':unicode,'default':None,
    'help':"Eligos result in txt file"})

##################################
######## Gene/Genome arguments #######
##################################

regions_opt=(("-reg","--region"),{'type':str,'default':None, 
             'help':"BED(BED6/BED12) format of genes or regions for " +
             "finding modification."})
ref_opt=(("-ref","--reference"),{'type':str,'default':None, 
             'help':"FA/FASTA format of the reference genome."})


############################
###### Text arguments ######
############################

rna_bams_opt=("rna_bams",{'nargs':'+','default':[],
              'help':"Direct RNA data in one or more sorted bam file(s)."})
cdna_bcf_opt=(("-bcf","--cdna_bcf"),{'nargs':'+','default':[],
              'help':"Direct cDNA data in sorted bcf file(s)."})
test_bams_opt=("--test_bams",{'nargs':'+','default':[],
              'help':"Test RNA/DNA data in one or more sorted bam files"})
control_bams_opt=("--ctrl_bams",{'nargs':'+','default':[],
              'help':"Control RNA/DNA data in one or more sorted bam files"})
bams_opt=("--bam",{'nargs':'+','default':[],
              'help':"Data in one or more sorted bam files"})
outdir=(("-o","--outdir"), {'type':str,'default':"results",
              'help':"Path of directory name to store output (default: %(default)s)"})
model_opt=(("-m","--model"), {'type':str,'default':None,
              'help':"Path of rBEM5+2 model in JSON file format"})
bname_opt=(('-p',"--prefix"), {'type':str,'default':None,
              'help':"Set the output file prefix (default: %(default)s)"})
out_tsv_opt=(("-o","--output"), {'type':str,'default':None,
              'help':"Path of file name to store BedGraph"})
signal_opt=(("-s","--signal"), {'type':str,'choices':["oddR", "ESB"],'default':'ESB',
              'help':"Signal type for BedGraph (default: %(default)s)"})
chbase_opt=(("-sb","--select_base"), {'type':str,'choices':["A","T","C","G","N"],'default':'N',
              'help':"Select output Base for BedGraph (default anybase: %(default)s)"})


############################
###### Int arguments ######
############################

threads_opt=(('-t','--threads',),{'type':int,'help':"Number of threads (default: %(default)s)",
            'default':3})
chunksize_opt=('--chunksize',{'type':int,'default':10000,
                'help':"Chanksize for multi-thread (default: %(default)s)"})
mapQ_opt=('--mapQ',{'type':int,'default':1,
                'help':"Skip alignments with MAPQ smaller than default: (%(default)s)"})
percent_identity_opt=('--percent_identity',{'type':float,'default':0,
                'help':"Skip alignments with %%identity smaller than default: (%(default)s)"})
query_cov_opt=('--query_coverage',{'type':float,'default':0,
                'help':"Skip alignments with %%query coverage smaller than default: (%(default)s)"})
aln_length_opt=('--aln_length',{'type':int,'default':200,
                'help':"Skip alignments with aligned length smaller than default: (%(default)s)"})
maxdepth_opt=('--max_depth',{'type':int,'default':10000,
                'help':"Maximum number of reads. default: (%(default)s)"})
mindepth_opt=('--min_depth',{'type':int,'default':20,
                'help':"Minimum number of reads. default: (%(default)s)"})
errr_opt=('--esb',{'type':float,'default':0.2,
                'help':"Minimum cut-off for ratio of error at specific base (ESB). default: (%(default)s)"})
oddr_opt=('--oddR',{'type':float,'default':2.5,
                'help':"Minimum cut-off for Odd ratio. default: (%(default)s)"})
pval_opt=('--pval',{'type':float,'default':0.05,
                'help':"p-value cut-off. default: (%(default)s)"})
adjPval_opt=('--adjPval',{'type':float,'default':1,
                'help':"Adjusted p-value cut-off. default: (%(default)s)"})
absLog2oddR=('--absLog2oddR',{'type':float,'default':1.0,
                'help':"Minimum cut-off for absolute log2 Odd ratio. default: (%(default)s)"})


###############################
###### Boolean arguments ######
###############################

force_opt=('--force',{'action':'store_true','help':"force delete tmp folder"})
supplementary_opt=('--supplementary',{'action':'store_true','help':"Also consider supplementary mapped reads"})
filt_homopolymer_opt=('--homopolymer', {'action':'store_true','help':"Filtering out homopolymer"})

###########################
###### Help argument ######
###########################

help_opt=(('-h','--help'), {
    'action':'help',
    'help':"Print this help message and exit"})
# logging_opt=('--verbose',{'help':"increase output verbosity",'action':"store_const",
#             'dest':"loglevel", 'const':logging.INFO})
quiet_opt=(('-q', '--quiet'), {
    'default':False, 'action':'store_true',
    'help':"Don't print status information."})

def add_misc_args(parser):
    misc_args = parser.add_argument_group('Miscellaneous Arguments')
    # misc_args.add_argument(*quiet_opt[0], **quiet_opt[1])
    misc_args.add_argument(*help_opt[0], **help_opt[1])
    return misc_args, parser

def add_multithreads_args(parser):
    multithreads_args = parser.add_argument_group('Multiprocessing Arguments')
    multithreads_args.add_argument(*threads_opt[0], **threads_opt[1])
    # multithreads_args.add_argument(chunksize_opt[0], **chunksize_opt[1])
    return multithreads_args, parser

######################################
###### Pre-processing parser ######
######################################

def get_map_preprocess_parser():
    example_text = '''example:

Commnad-line:
  %(prog)s map_preprocess -i input.bam -o output.bam 

 '''

    parser = argparse.ArgumentParser(
        description='Preprocess mapped reads from bam files.',
        epilog=example_text,
        add_help=False)
    req_args = parser.add_argument_group('Required Arguments')
    req_args.add_argument('-i',bam_opt[0], **bam_opt[1])

    criteria_args = parser.add_argument_group(title='Criteria options')
    criteria_args.add_argument('-suppl',supplementary_opt[0], **supplementary_opt[1])
    criteria_args.add_argument('-mapq',mapQ_opt[0], **mapQ_opt[1])
    criteria_args.add_argument('-pid',percent_identity_opt[0], **percent_identity_opt[1])
    criteria_args.add_argument('-qcov',query_cov_opt[0], **query_cov_opt[1])
    criteria_args.add_argument('-aln',aln_length_opt[0], **aln_length_opt[1])

    out_args = parser.add_argument_group(title='Output Argument')
    out_args.add_argument('-o',output_bam_opt[0], **output_bam_opt[1])

    multithreads_args = parser.add_argument_group('Multithreads Argument')
    multithreads_args.add_argument(*threads_opt[0], **threads_opt[1])

    misc_args, parser = add_misc_args(parser)

    return parser

def get_build_genedb_parser():
    example_text = '''example:\n\n
With output in directory:\n
  %(prog)s build_genedb -i file.bam -o results -regions file.bed 

 '''
    parser = argparse.ArgumentParser(
        description='Build bam files according to gene database',
        epilog=example_text,
        add_help=False)
    req_args = parser.add_argument_group('Required Arguments')
    req_args.add_argument('-i',bams_opt[0], **bams_opt[1])
    req_args.add_argument(*regions_opt[0], **regions_opt[1])

    out_args = parser.add_argument_group('Output Argument')
    out_args.add_argument(*outdir[0], **outdir[1])
    out_args.add_argument(sub_bam_dir_opt[0], **sub_bam_dir_opt[1])

    misc_args, parser = add_misc_args(parser)
    misc_args.add_argument(force_opt[0], **force_opt[1])

    # multithreads_args, parser = add_multithreads_args(parser)
    # fast5_args, misc_args, parser = add_default_args(parser)
    return parser

def get_rna_mod_parser():
    example_text = '''example:\n\n
With command:\n
  %(prog)s rna_mod -i file.bam -o results -reg file.bed -ref reference.fasta

 '''
    parser = argparse.ArgumentParser(
        description='Identify RNA modification against rBEM5+2 model',
        epilog=example_text,
        add_help=False)
    req_args = parser.add_argument_group(title='Required Arguments')
    req_args.add_argument('-i',bams_opt[0], **bams_opt[1])
    req_args.add_argument(*regions_opt[0], **regions_opt[1])
    req_args.add_argument(*ref_opt[0], **ref_opt[1])

    opt_args = parser.add_argument_group(title='Optional Arguments')
    opt_args.add_argument(*model_opt[0], **model_opt[1])
    opt_args.add_argument(*cdna_bcf_opt[0], **cdna_bcf_opt[1])

    out_args = parser.add_argument_group(title='Output Arguments')
    out_args.add_argument(*bname_opt[0], **bname_opt[1])
    out_args.add_argument(*outdir[0], **outdir[1])
    out_args.add_argument(sub_bam_dir_opt[0], **sub_bam_dir_opt[1])

    cri_args = parser.add_argument_group(title='Criteria Arguments')
    cri_args.add_argument(maxdepth_opt[0], **maxdepth_opt[1])
    cri_args.add_argument(mindepth_opt[0], **mindepth_opt[1])
    cri_args.add_argument(errr_opt[0], **errr_opt[1])
    cri_args.add_argument(oddr_opt[0], **oddr_opt[1])
    cri_args.add_argument(pval_opt[0], **pval_opt[1])
    cri_args.add_argument(adjPval_opt[0], **adjPval_opt[1])

    mult_args = parser.add_argument_group(title='Multiprocessing Arguments')
    mult_args.add_argument(*threads_opt[0], **threads_opt[1])

    misc_args, parser = add_misc_args(parser)
    misc_args.add_argument(force_opt[0], **force_opt[1])

    # multithreads_args, parser = add_multithreads_args(parser)
    # fast5_args, misc_args, parser = add_default_args(parser)
    return parser
    
def get_bedgraph_parser():
    example_text = '''example:\n\n
With output as File:\n
  %(prog)s bedgraph -i file.txt -s oddR

 '''

    parser = argparse.ArgumentParser(
        description='Bulid bedgraph according to the Eligos result',
        epilog=example_text,
        add_help=False)

    req_args = parser.add_argument_group(title='Required Arguments')
    req_args.add_argument('-i',eligos_result_opt[0], **eligos_result_opt[1])
    req_args.add_argument(*signal_opt[0], **signal_opt[1])

    out_args = parser.add_argument_group(title='Output Argument')
    out_args.add_argument(*bname_opt[0], **bname_opt[1])

    cri_args = parser.add_argument_group(title='Criteria Arguments')
    cri_args.add_argument(*chbase_opt[0], **chbase_opt[1])
    cri_args.add_argument(filt_homopolymer_opt[0], **filt_homopolymer_opt[1])
    cri_args.add_argument(mindepth_opt[0], **mindepth_opt[1])
    errr_opt[1]["default"] = 0
    cri_args.add_argument(errr_opt[0], **errr_opt[1])
    oddr_opt[1]["default"] = 0
    cri_args.add_argument(oddr_opt[0], **oddr_opt[1])
    pval_opt[1]["default"] = 1
    cri_args.add_argument(pval_opt[0], **pval_opt[1])
    adjPval_opt[1]["default"] = 1
    cri_args.add_argument(adjPval_opt[0], **adjPval_opt[1])

    misc_args, parser = add_misc_args(parser)

    return parser

def get_filter_parser():
    example_text = '''example:\n\n
With output as File:\n
  %(prog)s filter -i file.txt 

 '''
    parser = argparse.ArgumentParser(
        description='Filter for the Eligos result',
        epilog=example_text,
        add_help=False)

    req_args = parser.add_argument_group(title='Required Arguments')
    req_args.add_argument('-i',eligos_result_opt[0], **eligos_result_opt[1])

    out_args = parser.add_argument_group(title='Output Argument')
    out_args.add_argument(*bname_opt[0], **bname_opt[1])

    cri_args = parser.add_argument_group(title='Criteria Arguments')
    cri_args.add_argument(*chbase_opt[0], **chbase_opt[1])
    cri_args.add_argument(filt_homopolymer_opt[0], **filt_homopolymer_opt[1])
    cri_args.add_argument(mindepth_opt[0], **mindepth_opt[1])
    errr_opt[1]["default"] = 0
    cri_args.add_argument(errr_opt[0], **errr_opt[1])
    oddr_opt[1]["default"] = 0
    cri_args.add_argument(oddr_opt[0], **oddr_opt[1])
    pval_opt[1]["default"] = 1
    cri_args.add_argument(pval_opt[0], **pval_opt[1])
    adjPval_opt[1]["default"] = 1
    cri_args.add_argument(adjPval_opt[0], **adjPval_opt[1])

    misc_args, parser = add_misc_args(parser)

    return parser

def get_pair_diff_mod_parser():
    example_text = '''example:\n\n
With command:\n
  %(prog)s rna_mod -tBam test.file.bam -cBam ctrl.file.bam -p file_eligos_out -o results -regions file.bed 

 '''
    parser = argparse.ArgumentParser(
        description='Identify RNA modification against control condition',
        epilog=example_text,
        add_help=False)
    req_args = parser.add_argument_group('Required Arguments')
    req_args.add_argument('-tbam',test_bams_opt[0], **test_bams_opt[1])
    req_args.add_argument('-cbam',control_bams_opt[0], **control_bams_opt[1])
    req_args.add_argument(*regions_opt[0], **regions_opt[1])
    req_args.add_argument(*ref_opt[0], **ref_opt[1])

    opt_args = parser.add_argument_group('Optional Arguments')
    opt_args.add_argument(*model_opt[0], **model_opt[1])
    opt_args.add_argument(*cdna_bcf_opt[0], **cdna_bcf_opt[1])

    out_args = parser.add_argument_group('Output Arguments')
    out_args.add_argument(*bname_opt[0], **bname_opt[1])
    out_args.add_argument(*outdir[0], **outdir[1])
    out_args.add_argument(sub_bam_dir_opt[0], **sub_bam_dir_opt[1])

    cri_args = parser.add_argument_group('Criteria Arguments')
    cri_args.add_argument(maxdepth_opt[0], **maxdepth_opt[1])
    cri_args.add_argument(mindepth_opt[0], **mindepth_opt[1])
    cri_args.add_argument(errr_opt[0], **errr_opt[1])
    cri_args.add_argument(oddr_opt[0], **oddr_opt[1])
    cri_args.add_argument(pval_opt[0], **pval_opt[1])
    cri_args.add_argument(adjPval_opt[0], **adjPval_opt[1])

    mult_args = parser.add_argument_group('Multiprocessing Arguments')
    mult_args.add_argument(*threads_opt[0], **threads_opt[1])

    misc_args, parser = add_misc_args(parser)
    misc_args.add_argument(force_opt[0], **force_opt[1])

    return parser

def get_multi_samples_test_parser():
    example_text = '''example:\n\n
With command:\n
  %(prog)s multi_samples_test --test_mods s1_test_baseExt0.txt s2_test_baseExt0.txt --ctrl_mods s1_ctrl_baseExt0.txt s2_ctrl_baseExt0.txt

 '''
    parser = argparse.ArgumentParser(
        description='Perform multi-samples testing of rna_mod results',
        epilog=example_text,
        add_help=False)
    req_args = parser.add_argument_group('Required Arguments')
    req_args.add_argument("--test_mods", nargs="+", default=[],
        help="Two or more test samples from rna_mod results")
    req_args.add_argument("--ctrl_mods", nargs="+", default=[],
        help="Two or more control samples from rna_mod results")

    out_args = parser.add_argument_group('Output Arguments')
    out_args.add_argument(*bname_opt[0], **bname_opt[1])
    outdir[1]["default"] = '.'
    out_args.add_argument(*outdir[0], **outdir[1])

    cri_args = parser.add_argument_group(title='Criteria Arguments')
    cri_args.add_argument(*chbase_opt[0], **chbase_opt[1])
    cri_args.add_argument(filt_homopolymer_opt[0], **filt_homopolymer_opt[1])
    oddr_opt[1]["default"] = 0
    cri_args.add_argument(oddr_opt[0], **oddr_opt[1])
    absLog2oddR[1]["default"] = 0
    cri_args.add_argument(absLog2oddR[0], **absLog2oddR[1])
    pval_opt[1]["default"] = 1
    cri_args.add_argument(pval_opt[0], **pval_opt[1])
    adjPval_opt[1]["default"] = 1
    cri_args.add_argument(adjPval_opt[0], **adjPval_opt[1])

    misc_args, parser = add_misc_args(parser)

    return parser

if __name__ == '__main__':
    sys.stderr.write('This is a module. See commands with `eligos -h`')
    sys.exit(1)