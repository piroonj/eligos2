"""
.. module:: misc
   :platform: Unix, MacOSX
   :synopsis: module for micellaneous

.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com>

"""
#!/usr/bin/env python
import re,os,shutil,sys
import pysam
import pandas as pd
import pybedtools as bt
import datetime as dt
from tqdm import tqdm
import logging
import time, random

## logging information
def init_logs(args, tool="ELIGOS"):
    """Initiate log file and log arguments."""
    start_time = dt.fromtimestamp(time()).strftime('%Y%m%d_%H%M')
    logname = os.path.join(args.outdir, args.prefix + tool + "_" + start_time + ".log")
    handlers = [logging.FileHandler(logname)]
    if args.verbose:
        handlers.append(logging.StreamHandler())
    logging.basicConfig(
        format='%(asctime)s %(message)s',
        handlers=handlers,
        level=logging.INFO)
    logging.info('{} {} started with arguments {}'.format(tool, __version__, args))
    logging.info('Python version is: {}'.format(sys.version.replace('\n', ' ')))
    return logname

##### File & Folder management
def ensure_dir(file_path, force=False, sleep=False):
    directory = os.path.abspath(file_path) #abspath dirname
    ## random time to create new folder in case of submitting multiple jobs
    if sleep:
        time.sleep(random.random()+random.randint(1,5))
    if not os.path.exists(directory):
        os.makedirs(directory)
        return directory
    else:
        if force:
            shutil.rmtree(directory)
            os.makedirs(directory)
        return directory

##---------------------------------------
##### Interval management
def readBed(bedF):
    from pybedtools import BedTool
    if os.path.exists(bedF):
        beds = BedTool(bedF)
        if beds.field_count() < 6 or beds.file_type != 'bed':
            beddf = beds.to_dataframe()
            if beds.field_count() > 3:
                beddf['score'] = 0
                beddf['strand'] = "+"
                beds = BedTool.from_dataframe(beddf)

            elif beds.field_count() == 3:
                beddf['name'] = beddf['chrom'].astype(str)+":"+(beddf['start']+1).astype(str)+"-"+beddf['end'].astype(str)
                beddf['score'] = 0
                beddf['strand'] = "+"
                beds = BedTool.from_dataframe(beddf)
            else:
                exit("\nPlease provide BED6, or BED12 format.\n")
        mergedBed = beds.sort().merge(s=True,c='4',o='distinct')
        return(mergedBed)
    else:
        exit("There is no {} file".format(bedF))

def subset_bam_thread(subset_bam_params):
    count = 0
    (bam_path, chrom, start, end, subset_path, strand_label) = subset_bam_params
    outDir = os.path.dirname(subset_path)
    ensure_dir(outDir)
    if(os.path.isfile("{}.bai".format(subset_path))):
        pass
    else:
        with pysam.AlignmentFile(bam_path, "rb") as bamObj, pysam.AlignmentFile(subset_path, mode='wb', template=bamObj) as subset:
            for read in bamObj.fetch(chrom,start,end):
                if not read.is_supplementary and not read.is_unmapped and not read.is_duplicate:
                    if read.is_reverse and strand_label == "neg":
                        subset.write(read)
                    elif not read.is_reverse and strand_label == "pos":
                        subset.write(read)
                    count += 1
        pysam.index(subset_path)
