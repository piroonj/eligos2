"""
.. module:: bedgraph
   :platform: Unix, MacOSX
   :synopsis: Module for post processing of the Eligos result.

.. moduleauthor:: Visanu Wanchai <visanuw86@gmail.com> and Piroon Jenjaroenpun <piroonj@gmail.com>

"""
#!/usr/bin/env python
import argparse
import pandas as pd
import logging
import os
from _misc import ensure_dir

## Data preparation functions ######################

def get_args():
    example_text = '''Test bedGraph:

    With output as File:
      %(prog)s bedgraph -i file.txt -s oddR

     '''
    
    import _option_parsers
    parser = _option_parsers.get_bedgraph_parser()

    return(parser)

def _bedgraph_main(args):
    '''Build BedGraph from the Eligos result'''
    import numpy as np
    logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    logging.info('Build BedGraph from the Eligos result')
    logging.info(str(args))

    # input
    eligos_result = args.input
    # output
    if args.prefix is None:
        output = "{}".format(os.path.splitext(eligos_result)[0])
    else:
        output = "{}".format(args.prefix)
        ensure_dir(os.path.dirname(output))

    df_inf = pd.read_csv(eligos_result, sep='\t')
    filtering = (df_inf.oddR >= args.oddR) & (df_inf.ESB_test >= args.esb) & (df_inf.total_reads >= args.min_depth) & ((df_inf.pval <= args.pval) & (df_inf.adjPval <= args.adjPval))
    if args.homopolymer:
        filtering = filtering & (df_inf.homo_seq == "--")
    if args.select_base in ["A","T","C","G"]:
        filtering = filtering & (df_inf.ref == args.select_base)
        base_out = ".{}".format(args.select_base)
    else:
        base_out = ""
    out_list = []
    if args.signal == "ESB":
        output_t = "{}{}.ESB_test.bdg".format(output,base_out)
        selected_cols = ['chrom', 'start_loc', 'end_loc', "ESB_test"]
        df_inf.loc[filtering,selected_cols].to_csv(output_t, sep='\t', index=None, header=None)
        out_list.append(output_t)
        output_c = "{}{}.ESB_ctrl.bdg".format(output,base_out)
        selected_cols = ['chrom', 'start_loc', 'end_loc', "ESB_ctrl"]
        df_inf.loc[filtering,selected_cols].to_csv(output_c, sep='\t', index=None, header=None)
        out_list.append(output_c)
    elif args.signal == "oddR":
        output_o = "{}{}.oddR.bdg".format(output,base_out)
        selected_cols = ['chrom', 'start_loc', 'end_loc', "oddR"]
        df_inf = df_inf.replace([np.inf, -np.inf], np.nan).dropna(axis=0)
        df_inf.loc[filtering,selected_cols].to_csv(output_o, sep='\t', index=None, header=None)
        out_list.append(output_o)
    else:
        exit("\n\nThere is no chioce for {}\n").format(args.signal)
    # log INFO

    logging.info("Finish build bedGraph at {}\n".format(", ".join(out_list)))

if __name__ == '__main__':
    parser = get_args()
    args = parser.parse_args()
    print("test build BedGraph")

    if args.input is None:
        print('\n*************************************************************')
        print('Eligos error: Must provide the Eligos result')
        print('\n*************************************************************\n')
        parser.print_help()
    else:
        _bedgraph_main(args)
