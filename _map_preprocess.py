"""
.. module:: map_preprocess
   :platform: Unix, MacOSX
   :synopsis: module for Filtering mapped reads from bam file

.. moduleauthor:: Piroon Jenjaroenpun <piroonj@gmail.com> and Visanu Wanchai <visanuw86@gmail.com>

"""
#!/usr/bin/env python
import sys
import multiprocessing as mp
from tempfile import TemporaryDirectory
from _misc import *
from tqdm import tqdm
from _eligos_func import *
import logging

def get_args():
    example_text = '''Test map_preprocess:

Commnad-line:
  %(prog)s map_preprocess -i input.bam -o output.bam 


 '''
    import _option_parsers
    parser = _option_parsers.get_map_preprocess_parser()

    return(parser)


def bamFilterFileOut_parallel(args):
    # output
    output_bam=args.output_bam
    # number of processers
    num_threads = args.threads

    if output_bam != '-':
        ensure_dir(os.path.dirname(output_bam))

    with TemporaryDirectory() as temp_fol:
        params, list_chr = gen_params(temp_fol, args)

        list_read_stat = []
        # with tqdm(total=len(list_chr), desc="Mapping chromosomes ") as pbar:
        #     with mp.Pool(processes=num_threads) as pool:
        #         for out in pool.imap_unordered(split_PySam_filter_by_chromosome, params):
        #             list_read_stat.append(out)
        #             pbar.update()
        with futures.ProcessPoolExecutor(max_workers=num_threads) as executor:
            with tqdm(total=len(list_chr), desc="Mapping chromosomes ") as pbar:
                for out in executor.map(split_PySam_filter_by_chromosome, params):
                    list_read_stat.append(out)
                    pbar.update()

        total = sum([x[0] for x in list_read_stat])
        filReads = sum([x[1] for x in list_read_stat])

        merge_sort_reads(temp_fol, output_bam)
    
    if output_bam != '-':
        logging.info("Indexing result ...")
        pysam.index(output_bam, "{}.bai".format(output_bam))

    ## log INFO
    logging.info("Total mapped (not include secondary): {}".format(total))
    if filReads == 0 or total == 0:
        percentReadLeftover = "NA"
    else:
        percentReadLeftover = "{:.1f}".format(100*(float(filReads)/total))
    logging.info("Mapped reads after filtering: {}, ({}%)".format(filReads, percentReadLeftover))


def _map_preprocess_main(args):
    '''Reads Mapped proprocessing'''
    ## log INFO
    # if not args.quiet:
    logging.basicConfig(format='%(asctime)s %(message)s',level=logging.INFO)
    logging.info('Reads Mapped proprocessing')
    logging.info(str(args))
    
    if args.output_bam:
        bamFilterFileOut_parallel(args)
    else:
        args.output_bam = ".".join([os.path.splitext(args.bam)[0], 'preprocess.bam'])
        bamFilterFileOut_parallel(args)

if __name__ == "__main__":
    print("test map_preprocess")

    parser = get_args()
    args = parser.parse_args()
    print("test pre-process mapped reads from bam files")

    if args.bam is None:
        print('\n*************************************************************')
        print('Eligos error: Must provide BAM file')
        print('\n*************************************************************\n')
        parser.print_help()
    else:
        _map_preprocess_main(args)


